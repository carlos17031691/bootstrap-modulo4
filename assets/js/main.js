$(function() {
    $("[data-toggle='tooltip']").tooltip()
})
$('.carousel').carousel({
    interval: 3000
})

$('#myModal').on('show.bs.modal', function(e1) {
    console.log('La modal se muestra')
   $('#'+e1.relatedTarget.id).attr('disabled', true).removeClass('btn-primary').addClass('btn-outline-primary')
   $('#myModal').on('hide.bs.modal', function(e2) {
    console.log('La modal se oculta')
    $('#'+e1.relatedTarget.id).attr('disabled', false).removeClass('btn-outline-primary').addClass('btn-primary')
})
   
})
$('#myModal').on('shown.bs.modal', function(e) {
    console.log('La modal se muestró')
})

$('#myModal').on('hidden.bs.modal', function(e) {
    console.log('La modal se ocultó')
})