module.exports = function (grunt){
    require('time-grunt')(grunt)
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    })
    grunt.initConfig({
        sass: {
            dist: {
                files: {
                'assets/css/main.css': 'assets/css/main.scss', 
                }
            }
        },

        watch: {
            files: ['assets/css/*.scss'],
            tasks: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'assets/css/*.css',
                        '*.html',
                        'assets/js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './'
                    }
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'assets/images/*.{png,jpeg,jpg,gif}',
                    dest: 'dist/'
                }]
            }
        },
        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: 'node_modules/open-iconic/font/fonts',
                        src: ['*.*'],
                        dest: 'dist/assets/fonts'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'node_modules/font-awesome/fonts/',
                        src: ['*.*'],
                        dest: 'dist/assets/fonts'
                    }
                ]
            }
        },
        clean: {
            biuld: {
                src: ['dist']
            }
        },
        cssmin: {
            dist: {}
        },
        uglify: {
            dist: {}
        },
        filerev: {
            options: {
                encoding: 'utf8',
                algoritmo: 'md5',
                length: 20
            },
            release : {
                files: [{
                    src: [
                        'dist/assets/js/*.js',
                        'dist/assets/css/*.css'
                    ]
                }]
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'about.html', 'contact.html', 'prices.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase:  false
                                }
                            }
                        }]
                    }
                }
            }
        },
        usemin: {
            html: ['dist/index.html', 'dist/about.html','dist/contact.html','dist/prices.html'],
            options: {
                assetsDir: ['dist/', 'dist/assets/css','dist/assets/js']
            }
        }
    })

    grunt.registerTask('css', ['sass'])
    grunt.registerTask('default', ['browserSync', 'watch'])
    grunt.registerTask('img:compress', ['imagemin'])
    grunt.registerTask('build',  [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ])

}